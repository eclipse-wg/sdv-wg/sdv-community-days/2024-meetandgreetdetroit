**SDV WG Meet&Greet Detroit**

**When**: 28th March. 9-12:30 with networking lunch

**Where**: GM Tech Center, 30001 Van Dyke Ave, Warren, MI 48093

**Registration**: via [meetup](https://www.meetup.com/virtual-eclipse-community-meetup/events/299181512/?utm_medium=referral&utm_campaign=share-btn_savedevents_share_modal&utm_source=link)  

[Group Photo](https://drive.google.com/file/d/1ArhJ14zIWmLSba-iTRLcbLKQkJZFkCoK/view?usp=sharing)


| Time | Session Name | Presenter |
| ------ | ------ | ------ |
| 8:30-9:00 | Welcome and coffee | |
| 9:00-10:30 | **Presentations 1.5h** | |
| 9:00-9:15 | [Welcome & introduction to the Eclipse SDV WG](https://drive.google.com/file/d/13L1h5Gf1EeTkigNzoTvC79R1wzKzuU0e/view?usp=drive_link) | Sara (EF)|  
| 9:15-9:45 | [Welcome for the Host: Eclipse Foundation & GM](https://docs.google.com/presentation/d/1f60TVQD0aefzyy6jXd7ofO11Rw4c2M10/edit?usp=drive_link&ouid=111179048981834231696&rtpof=true&sd=true)  | Dan Nicholson (GM) - Vice President of Strategic Technology Initiatives|  
| 9:45-10:15 | [SDV WG collaboration around Eclipse uProtocol](https://docs.google.com/presentation/d/1J5X-wg6_kfy33HUW1e7iqzkHtdT0qFrS/edit?usp=drive_link&ouid=111179048981834231696&rtpof=true&sd=true) | Steven Hartley (GM) - Technical Fellow |  
| 10:15-10:30 | [Eclipse EDV WG: a Continental Prospective](https://docs.google.com/presentation/d/1AIfvCffhTIMtA5vufea1KSyb8O_ZvvNZ/edit?usp=drive_link&ouid=111179048981834231696&rtpof=true&sd=true) | Martin Schleicher (Conti) - Head of Software Strategy| 
| 10:30-11 | Coffee break & Networking 30 mins | |
| 11-12:30 | **Presentations 1h30** | |  
| 11:00-11:30 | [Software Orchestration work with Kanto Update Manager framework](https://docs.google.com/presentation/d/1_IxXKgfJYA40LcQiaNN7NoVTmxeo0pk9/edit?usp=drive_link&ouid=111179048981834231696&rtpof=true&sd=true) | Carlton Bale (Cummins) - Director - Digital Strategy and Product Planning |
| 11:30-12:00 | [The connected car, a multiplayer adventure](https://drive.google.com/file/d/1fjvi5lKDFY1TSFElB6qkUu03nglOLFC4/view?usp=sharing)  | Andre Larberg	(IAV) - Head of Software Defined Vehicle & Open Source|
| 12:00-12:30 | [Working Together: Red Hat's Community Approach to Automotive Software"](https://docs.google.com/presentation/d/1L5oVT-8t845XLrebCqJhlVWN0uajd_KP/edit?usp=drive_link&ouid=111179048981834231696&rtpof=true&sd=true)  | Jeffry Osier-Mixon (RedHat) -Senior Principal Community Architect |
| 12:30-12:45 | Closing remarks  | Sara & host |
| 12:45-13:30 | Networking Lunch | |


